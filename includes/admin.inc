<?php

/**
 * @file
 * Administration settings for the vertical_tabs_visibility module.
 */

/**
 * Callback for vertical tabs visibility settings.
 */
function _vertical_tabs_visibility_settings($form, &$form_state) {
  
  $types = node_type_get_names();

  $form['vertical_tabs_visibility_hide'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Hide vertical tabs'),
    '#options' => $types,
    '#description' => t('Vertical tabs at node add/edit form will be disabled for checked content types.'),
    '#default_value' => variable_get('vertical_tabs_visibility_hide', array()),
  );
  
  $form['vertical_tabs_visibility_hide_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Hide mode'),
    '#options' => array(
      'render' => t('Don\'t render vertical tabs.'),
      'css' => t('Hide vertical tabs with CSS.'),
    ),
    '#description' => t('The way vertical tabs will be hidden.'),
    '#default_value' => variable_get('vertical_tabs_visibility_hide', array()),
  );
  
  return system_settings_form($form);
}
